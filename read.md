<h1>Technical Test Shopee</h1>

1. Test Case <a href="https://gitlab.com/dewabt8/cicd-django">link CICD</a> 
2. Coding Test
    
    A. Create Rest API using Golang / Rust / Python to serve CRUD <a href="https://gitlab.com/dewabt8/crud-shopee">link CRUD</a>
    1. POST → /api/v1/generate – Download dummy csv from internet 
        <br />
        user generate csv from internet
        <br />
        ![Alt text ](img/crud-api-generate-1.jpg) 
        <br />
        program save csv to folder download/
        <br />
        ![Alt text](img/crud-api-generate-2.jpg)
        <br />
    2. GET → /api/v1/download – Serve the dummy csv to user as a octet-stream
        <br />
        user download csv from program
        <br />
        ![Alt text](img/crud-api-download-1.jpg)
        <br />
    3. GET → /api/v1/send_mq → Send data to message queue 
        <br />
        user send csv to message queue (kafka)
        <br />
        ![Alt text](img/crud-api-mq-1.jpg)
        <br />
        <br />
        this is cluster kafka for message queue
        ![Alt text](img/crud-api-mq-2.jpg)
        <br />
        <br />
        data got consume 
        <br />
        ![Alt text](img/crud-api-mq-3.jpg)
        <br />
    
    
    B. Create Consumer for message queue (ex: kafka) <a href="https://gitlab.com/dewabt8/mq-shopee">link message queure</a>
    
    1. Consume the data from message queue
        <br />
        ![Alt text](img/message-queue-1.jpg)
        <br />
    2. Serve it as recorder prometheus exposition
        <br />
        ![Alt text](img/message-queue-2.jpg)
        <br />
        
    C. Deploy prometheus and grafana stack in local, and create the grafana dashboard for the data saved by prometheus. <a href="https://gitlab.com/dewabt8/mq-shopee">link prometheus grafana</a>

    1. prometheus running
        <br />
        ![Alt text](img/promtheus-grafana-1.jpg)
        <br />
        2. grafana running
        <br />
        ![Alt text](img/promtheus-grafana-2.jpg)
        <br />
    